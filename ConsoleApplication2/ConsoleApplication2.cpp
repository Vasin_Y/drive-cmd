// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <winioctl.h>

#include <stddef.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <windows.h>
#include <iostream>
#include "setupapi.h"
#include "assert.h"
#include <vector>
#include <iomanip>

#define ERR_SUCCESS      0
#define ERR_FAIL         1

static HANDLE handle = INVALID_HANDLE_VALUE;
static int timeout = 30;

#define SCSI_IOCTL_DATA_OUT               0
#define SCSI_IOCTL_DATA_IN                1

using namespace std;

HANDLE ScsiOpen(const char driveLatter);

unsigned char buff[0x2000];
unsigned char buff_resp[0x2000];

typedef struct _SCSI_PASS_THROUGH_DIRECT {
	USHORT  Length;
	UCHAR  ScsiStatus;
	UCHAR  PathId;
	UCHAR  TargetId;
	UCHAR  Lun;
	UCHAR  CdbLength;
	UCHAR  SenseInfoLength;
	UCHAR  DataIn;
	ULONG  DataTransferLength;
	ULONG  TimeOutValue;
	PVOID  DataBuffer;
	ULONG  SenseInfoOffset;
	UCHAR  Cdb[16];
} SCSI_PASS_THROUGH_DIRECT, *PSCSI_PASS_THROUGH_DIRECT;



static int CallSptd(unsigned char dir, unsigned timeout, unsigned char *cdb, unsigned cdb_size, void *buf, unsigned data_size)
{
	uint8_t cmd[0x50];
	memset(cmd, 0, 0x50);

	SCSI_PASS_THROUGH_DIRECT *sptd = (SCSI_PASS_THROUGH_DIRECT *)cmd;
	sptd->Length = sizeof(SCSI_PASS_THROUGH_DIRECT);
	sptd->TimeOutValue = timeout;
	sptd->PathId = 0;
	sptd->TargetId = 1;
	sptd->Lun = 0;
	sptd->CdbLength = cdb_size;
	sptd->SenseInfoLength = 0x18;
	sptd->DataIn = dir;
	sptd->DataTransferLength = data_size;
	sptd->DataBuffer = buf;
	sptd->SenseInfoOffset = 0x30;
	memcpy(sptd->Cdb, cdb, cdb_size);
	DWORD BytesReturned = 0;
	if (!DeviceIoControl(handle, 0x4D014, cmd, 0x50, cmd, 0x50, &BytesReturned, 0))
	{
		std::cout << "DeviceIoControl Error" << std::endl;
		return GetLastError();
	}
	return sptd->ScsiStatus;
}


int ScsiOut(unsigned char *cdb, unsigned cdb_size, void *buf, unsigned data_size)
{
	return CallSptd(SCSI_IOCTL_DATA_OUT, timeout, cdb, cdb_size, buf, data_size);
}

int ScsiIn(unsigned char *cdb, unsigned cdb_size, void *buf, unsigned data_size)
{
	return CallSptd(SCSI_IOCTL_DATA_IN, timeout, cdb, cdb_size, buf, data_size);
}

int GetDriveGeometry(LPWSTR wszPath, DISK_GEOMETRY *pdg);
#define wszDrive L"\\\\.\\PhysicalDrive0"

void hexdump(char* Buff, int len);

int main()
{
	DISK_GEOMETRY pdg = { 0 }; // disk drive geometry structure
	int bResult = FALSE;      // generic results flag
	long long DiskSize = 0;    // size of the drive, in bytes

	bResult = GetDriveGeometry(wszDrive, &pdg);

	if (bResult)
	{
		wprintf(L"Drive path      = %ws\n", wszDrive);
		wprintf(L"Cylinders       = %I64d\n", pdg.Cylinders);
		wprintf(L"MEDIA_TYPE      = %d\n", pdg.MediaType);
		wprintf(L"Tracks/cylinder = %ld\n", (ULONG)pdg.TracksPerCylinder);
		wprintf(L"Sectors/track   = %ld\n", (ULONG)pdg.SectorsPerTrack);
		wprintf(L"Bytes/sector    = %ld\n", (ULONG)pdg.BytesPerSector);

		DiskSize = pdg.Cylinders.QuadPart * (ULONG)pdg.TracksPerCylinder *
			(ULONG)pdg.SectorsPerTrack * (ULONG)pdg.BytesPerSector;
		wprintf(L"Disk size       = %I64d (Bytes)\n"
			L"                = %.2f (Gb)\n",
			DiskSize, (double)DiskSize / (1024 * 1024 * 1024));
	}
	else
	{
		wprintf(L"GetDriveGeometry failed. Error %ld.\n", GetLastError());
	}


	handle = ScsiOpen('F');

	if (handle == INVALID_HANDLE_VALUE)
		std::cout << "INVALID HANDLE" << std::endl;
	else
		std::cout << "HANDLE: " << std::hex << handle << std::endl;

	memset(buff, 0, sizeof(buff));

	// Scsi_InQ
	buff[0] = 0x12; // cmd
	buff[1] = 0x00;
	buff[2] = 0x00;
	buff[3] = 0x00;
	buff[4] = 0x40;
	ScsiIn(buff, 0x5, buff_resp, 0x40);
	for (int i = 0; i < 0x40; i++)
		cout << " "  << hex << int(buff_resp[i]);

	cout << endl;
	
	CloseHandle(handle);

	hexdump((char*)buff_resp, 0x35);

	getchar();
    return 0;
}

void hexdump(char* Buff, int len)
{
	unsigned long address = 0;
	int nread = 0x10;
	cout << hex << setfill('0');

	int nline = len / nread;
	int nresidue = len % nread;
	int idx = 0;
	char* buff = Buff;

	// print whole lines
	while (idx < nline)
	{
		// Show the address
		cout << setw(8) << address;

		// Show the hex codes
		for (int i = 0; i < 16; i++)
		{
			if (i % 8 == 0) cout << ' ';
			if (i < nread)
				cout << ' ' << setw(2) << (unsigned int)(unsigned char)buff[i];
			else
				cout << "   ";
		}

		// Show printable characters
		cout << "  ";
		for (int i = 0; i < nread; i++)
		{
			if (buff[i] < 32) cout << '.';
			else cout << buff[i];
		}

		cout << "\n";
		address += 0x10;
		idx++;
		buff += 0x10;
	}

	if (nresidue) 
	{		
		// Show the address
		cout << setw(8) << address;

		// Show the hex codes
		for (int i = 0; i < nresidue; i++)
		{
			if (i % 8 == 0) cout << ' ';
			if (i < nread)
				cout << ' ' << setw(2) << (unsigned int)(unsigned char)buff[i];
		}
		
		for (int i = nresidue; i < nread; i++)
		{
			if (i % 8 == 0) cout << ' ';
			if (i < nread)
				cout << "   ";
		}

		// Show printable characters
		cout << "  ";
		for (int i = 0; i < nresidue; i++)
		{
			if (buff[i] < 32) cout << '.';
			else cout << buff[i];
		}
	}
	


}

int GetDriveGeometry(LPWSTR wszPath, DISK_GEOMETRY *pdg)
{
	HANDLE hDevice = INVALID_HANDLE_VALUE;  // handle to the drive to be examined 
	BOOL bResult = FALSE;                 // results flag
	DWORD junk = 0;                     // discard results

	hDevice = CreateFileW(wszPath,          // drive to open
		0,                // no access to the drive
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,    // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDevice == INVALID_HANDLE_VALUE)    // cannot open the drive
	{
		return (FALSE);
	}

	bResult = DeviceIoControl(hDevice,                       // device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
		NULL, 0,                       // no input buffer
		pdg, sizeof(*pdg),            // output buffer
		&junk,                         // # bytes returned
		(LPOVERLAPPED)NULL);          // synchronous I/O

	CloseHandle(hDevice);

	return (bResult);
}

HANDLE ScsiOpen(const char driveLatter)
{
	char path[] = "\\\\.\\T:";
	//char path[] = "\\\\.\\PhysicalDrive3";
	path[4] = driveLatter;

	HANDLE hf = CreateFileA(path,
		GENERIC_READ | GENERIC_WRITE, 
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		0, //lpSecurityAttributes
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		0 //hTemplateFile
	);

	if (hf == INVALID_HANDLE_VALUE)
		CloseHandle(hf);

	hf = CreateFileA(path,
		GENERIC_READ | GENERIC_WRITE, 
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		0, //lpSecurityAttributes
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		0 //hTemplateFile
	);

	return hf;
}